package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class HotelService {
	private final HotelRepository hotelRepository;

	public HotelService(HotelRepository hotelRepository) {
		this.hotelRepository = hotelRepository;
	}

	public void delete(UUID hotelId) {
		if (!isHotelOwnerOrAdmin(UserService.getCurrent(), hotelRepository.getById(hotelId).orElseThrow())) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not hotel owner or admin");
		}
		boolean wasDeleted = hotelRepository.delete(hotelId);
		if (!wasDeleted) {
			throw new NoSuchElementException();
		}
	}

	public List<Hotel> getAll() {
		return hotelRepository.getHotels();
	}

	public Hotel update(Hotel hotel) {
		if (!isHotelOwnerOrAdmin(UserService.getCurrent(), hotel)) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not hotel owner or admin");
		}
		getById(hotel.getId());
		return hotelRepository.save(hotel);
	}

	public Hotel create(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public Hotel getById(UUID hotelId) {
		return hotelRepository.getById(hotelId).orElseThrow();
	}

	private boolean isHotelOwnerOrAdmin(User user, Hotel hotel) {
		boolean isHotelOwner = hotel.getOwnerId() == user.getId();
		boolean isAdmin = user.getAuthorities().contains(UserRole.ADMIN);
		return isHotelOwner || isAdmin;
	}
}
