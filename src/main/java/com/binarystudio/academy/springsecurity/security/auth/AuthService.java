package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}

        return AuthResponse.of(
                jwtProvider.generateAccessToken(userDetails),
                jwtProvider.generateRefreshToken(userDetails));
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}

    public AuthResponse registerUser(RegistrationRequest registrationRequest) {
        userService.save(User.of(
                registrationRequest.getLogin(),
                registrationRequest.getEmail(),
                passwordEncoder.encode(registrationRequest.getPassword()),
                Set.of(UserRole.USER)));

        var userDetails = userService.loadUserByUsername(registrationRequest.getLogin());

        return AuthResponse.of(
                jwtProvider.generateAccessToken(userDetails),
                jwtProvider.generateRefreshToken(userDetails));
    }

    public AuthResponse refreshTokenPair(RefreshTokenRequest refreshTokenRequest) {
        String refreshToken = refreshTokenRequest.getRefreshToken();
        String userLogin = jwtProvider.getLoginFromToken(refreshToken);
        var userDetails = userService.loadUserByUsername(userLogin);

        return AuthResponse.of(
                jwtProvider.generateAccessToken(userDetails),
                jwtProvider.generateRefreshToken(userDetails));
    }

    public AuthResponse changePasswordForCurrentUser(PasswordChangeRequest passwordChangeRequest) {
        User user = UserService.getCurrent();
        return changePassword(user, passwordChangeRequest.getNewPassword());
    }

    public AuthResponse changePasswordByToken(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        String token = forgottenPasswordReplacementRequest.getToken();
        User user = userService.loadUserByUsername(jwtProvider.getLoginFromToken(token));
        return changePassword(user, forgottenPasswordReplacementRequest.getNewPassword());
    }

    private AuthResponse changePassword(User user, String newRawPassword) {
        user.setPassword(passwordEncoder.encode(newRawPassword));

        return AuthResponse.of(
                jwtProvider.generateAccessToken(user),
                jwtProvider.generateRefreshToken(user));
    }

    public String getPasswordResetToken(String email) {
	    User user = userService.loadUserByEmail(email);
	    return jwtProvider.generateAccessToken(user);
    }
}
